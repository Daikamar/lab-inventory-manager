package tv.sweeney.labinventory.repository;

import com.arris.ssd.labinventory.model.Lab
//import com.arris.ssd.labinventory.model.NodeType
import com.arris.ssd.labinventory.model.Rack
//import com.arris.ssd.labinventory.model.VirtualHost
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.repository.findByIdOrNull

@DataJpaTest
@Tag("IntegrationTest")
class RackRepositoryTests @Autowired constructor(
        val entityManager:TestEntityManager,
        val rackRepository: RackRepository,
        val nodeRepository: NodeRepository) {

    @Test
    fun `When findByIdOrNull then return Rack`() {
        val lab = Lab("Lab-001", "USA", "PA", "Philadelphia", "Building 1")
        entityManager.persist(lab)
        val rack = Rack("Rack-001", 48, lab)
        entityManager.persist(rack)
        entityManager.flush()
        val found = rackRepository.findByIdOrNull(rack.id!!)
        assertThat(found).isEqualTo(rack)
    }

    @Test
    fun `When findByName then return Rack`() {
        val lab = Lab("Lab-001", "USA", "PA", "Philadelphia", "Building 1")
        entityManager.persist(lab)
        val rack = Rack("Rack-001", 48, lab)
        entityManager.persist(rack)
        entityManager.flush()
        val found = rackRepository.findByName(rack.name)
        assertThat(found).isEqualTo(rack)
    }

    @Test
    fun `When findBySize then return Rack`() {
        val lab = Lab("Lab-001", "USA", "PA", "Philadelphia", "Building 1")
        entityManager.persist(lab)
        val rack = Rack("Rack-001", 48, lab)
        entityManager.persist(rack)
        entityManager.flush()
        val found = rackRepository.findBySize(rack.size)
        assertThat(found).isEqualTo(rack)
    }

    @Test
    fun `When findByNodeType then return Rack`() {
        val lab = Lab("Lab-001", "USA", "PA", "Philadelphia", "Building 1")
        entityManager.persist(lab)
        val rack = Rack("Rack-001", 48, lab)
        entityManager.persist(rack)
        entityManager.flush()
        val found = rackRepository.findByLab(rack.lab)
        assertThat(found).isEqualTo(rack)
    }

//    @Test
//    fun `Assert Node-less Open Positions Properly Reported`() {
//        val lab = Lab("Lab-001", "USA", "PA", "Philadelphia", "Building 1")
//        entityManager.persist(lab)
//        val rack = Rack("Rack-001", 5, lab)
//        entityManager.persist(rack)
//        rackRepository.save(rack)
//        val virtualHost = VirtualHost("VHost-001", NodeType.VIRTUAL_HOST, rack, 2, 3)
//        entityManager.persist(virtualHost)
//        nodeRepository.save(virtualHost)
//        entityManager.flush()
//        val rack2 = rackRepository.findByName("Rack-001")
//        val nodes = nodeRepository.findAll()
//        assertThat(1).isEqualTo(1)
//        assertThat(rack.openPositions().first()).isEqualTo(1)
//        assertThat(rack.openPositions().last()).isEqualTo(5)
//        assertThat(rack.openPositions().size).isEqualTo(3)
//    }
}
