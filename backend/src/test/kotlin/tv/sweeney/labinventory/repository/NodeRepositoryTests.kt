package tv.sweeney.labinventory.repository

import com.arris.ssd.labinventory.model.Lab
import com.arris.ssd.labinventory.model.Node
import com.arris.ssd.labinventory.model.NodeType
import com.arris.ssd.labinventory.model.Rack
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.data.repository.findByIdOrNull

@DataJpaTest
@Tag("IntegrationTest")
class NodeRepositoryTests @Autowired constructor(
        val entityManager: TestEntityManager,
        val nodeRepository: NodeRepository) {

    @Test
    fun `When findByIdOrNull then return Node`() {
        val lab = Lab("Lab-001", "USA", "PA", "Philadelphia", "Building 1")
        entityManager.persist(lab)
        val rack = Rack("Rack-001", 48, lab)
        entityManager.persist(rack)
        val node = Node("Node-001", NodeType.VIRTUAL_HOST)
        entityManager.persist(node)
        entityManager.flush()
        val found = nodeRepository.findByIdOrNull(node.id!!)
        assertThat(found).isEqualTo(node)
    }

    @Test
    fun `When findByName then return Node`() {
        val lab = Lab("Lab-001", "USA", "PA", "Philadelphia", "Building 1")
        entityManager.persist(lab)
        val rack = Rack("Rack-001", 48, lab)
        entityManager.persist(rack)
        val node = Node("Node-001", NodeType.VIRTUAL_HOST)
        entityManager.persist(node)
        entityManager.flush()
        val found = nodeRepository.findByName(node.name)
        assertThat(found).isEqualTo(node)
    }

}
