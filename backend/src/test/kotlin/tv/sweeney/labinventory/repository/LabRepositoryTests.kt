package tv.sweeney.labinventory.repository;

import com.arris.ssd.labinventory.model.Lab
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.data.repository.findByIdOrNull

@DataJpaTest
@Tag("IntegrationTest")
class LabRepositoryTests @Autowired constructor(
        val entityManager: TestEntityManager,
        val labRepository: LabRepository) {

    @Test
    fun `When findByIdOrNull then return Lab`() {
        val lab = Lab("Lab-001", "USA", "PA", "Philadelphia", "Building 1")
        entityManager.persist(lab)
        entityManager.flush()
        val found = labRepository.findByIdOrNull(lab.id!!)
        assertThat(found).isEqualTo(lab)
    }

    @Test
    fun `When findByName then return Lab`() {
        val lab = Lab("Lab-001", "USA", "PA", "Philadelphia", "Building 1")
        entityManager.persist(lab)
        entityManager.flush()
        val found = labRepository.findByName(lab.name)
        assertThat(found).isEqualTo(lab)
    }
}
