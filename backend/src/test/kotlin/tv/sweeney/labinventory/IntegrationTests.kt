//package com.arris.ssd.labinventory
//
//import org.assertj.core.api.Assertions.assertThat
//import org.junit.jupiter.api.AfterAll
//import org.junit.jupiter.api.BeforeAll
//import org.junit.jupiter.api.Tag
//import org.junit.jupiter.api.Test
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.boot.test.context.SpringBootTest
//import org.springframework.boot.test.web.client.TestRestTemplate
//import org.springframework.boot.test.web.client.getForEntity
//import org.springframework.http.HttpStatus
//
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@Tag("IntegrationTest")
//class IntegrationTests(@Autowired val restTemplate: TestRestTemplate) {
//
//    @Test
//    fun `Assert index title and status code`() {
//        val entity = restTemplate.getForEntity<String>("/")
//        assertThat(entity.statusCode).isEqualTo(HttpStatus.OK)
//        assertThat(entity.body).contains("<h1>Lab Inventory</h1>")
//    }
//
//    @Test
//    fun `Assert nodeTypes title and status code`() {
//        val entity = restTemplate.getForEntity<String>("/nodeTypes")
//        assertThat(entity.statusCode).isEqualTo(HttpStatus.OK)
//        assertThat(entity.body).contains("<h1>Node Types</h1>")
//    }
//
//}
