package tv.sweeney.labinventory.model

import com.arris.ssd.labinventory.util.ipIntToString
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test

@Tag("UnitTest")
class NetworkTests {

    @Test
    fun `Create Network from String`() {
        val network = Network(
            "Network Name",
            "192.168.1.0",
            "255.255.255.0",
            "192.168.1.1",
            "192.168.1.255"
        )
        assertThat(network.name).isEqualTo("Network Name")
        assertThat(ipIntToString(network.baseAddress)).isEqualTo("192.168.1.0")
        assertThat(ipIntToString(network.subnetAddress)).isEqualTo("255.255.255.0")
        assertThat(ipIntToString(network.gatewayAddress)).isEqualTo("192.168.1.1")
        assertThat(ipIntToString(network.broadcastAddress)).isEqualTo("192.168.1.255")
    }
}
