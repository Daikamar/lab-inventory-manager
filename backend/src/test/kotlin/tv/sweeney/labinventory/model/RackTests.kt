package tv.sweeney.labinventory.model

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatIllegalArgumentException
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test

@Tag("UnitTest")
class RackTests {

    @Test
    fun `Create Rack`() {
        val lab = Lab("Lab-001", "USA", "PA", "Philadelphia", "Building 1")
        val rack = Rack("Rack-001", 48, lab)
        assertThat(rack.name).isEqualTo("Rack-001")
        assertThat(rack.size).isEqualTo(48)
        assertThat(rack.lab).isEqualTo(lab)
    }

//    @Test
//    fun `Assert Node-less Open Positions Properly Reported`() {
//        val lab = Lab("Lab-001", "USA", "PA", "Philadelphia", "Building 1")
//        val rack = Rack("Rack-001", 5, lab)
//        assertThat(rack.openPositions().first()).isEqualTo(1)
//        assertThat(rack.openPositions().last()).isEqualTo(5)
//    }

//    @Test
//    fun `Assert Exception Thrown When VirtualHost Doesn't Fit In Rack`() {
//        val lab = Lab("Lab-001", "USA", "PA", "Philadelphia", "Building 1")
//        val rack = Rack("Rack-001", 48, lab)
//        assertThat(VirtualHost("VHost-001", NodeType.VIRTUAL_HOST, 2, 47, rack)).isNotNull
//        assertThatIllegalArgumentException().isThrownBy { VirtualHost("VHost-001", NodeType.VIRTUAL_HOST, 2, 48, rack) }
//        assertThatIllegalArgumentException().isThrownBy { VirtualHost("VHost-001", NodeType.VIRTUAL_HOST, 2, 50, rack) }
//    }
}
