package tv.sweeney.labinventory.model

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test

@Tag("UnitTest")
class NodeTests {

    @Test
    fun `Create Node`() {
        val node = Node("Node-001", NodeType.VIRTUAL_HOST)
        Assertions.assertThat(node.name).isEqualTo("Node-001")
        Assertions.assertThat(node.nodeType).isEqualTo(NodeType.VIRTUAL_HOST)
    }
}
