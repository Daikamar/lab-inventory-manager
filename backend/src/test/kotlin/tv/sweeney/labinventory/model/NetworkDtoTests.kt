package tv.sweeney.labinventory.model

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test

@Tag("UnitTest")
class NetworkDtoTests {

    @Test
    fun `Create NetworkDto from String`() {
        val networkDto = NetworkDto(
            "Network Name",
            "192.168.1.0",
            "255.255.255.0",
            "192.168.1.1",
            "192.168.1.255"
        )
        assertThat(networkDto.name).isEqualTo("Network Name")
        assertThat(networkDto.baseAddress).isEqualTo("192.168.1.0")
        assertThat(networkDto.subnetAddress).isEqualTo("255.255.255.0")
        assertThat(networkDto.gatewayAddress).isEqualTo("192.168.1.1")
        assertThat(networkDto.broadcastAddress).isEqualTo("192.168.1.255")
    }
}
