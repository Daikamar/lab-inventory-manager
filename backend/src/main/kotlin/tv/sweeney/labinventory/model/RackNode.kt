package tv.sweeney.labinventory.model

interface RackNode {
    val size: Int
    val rackPosition: Int
}
