package tv.sweeney.labinventory.model

import com.arris.ssd.labinventory.util.ipStringToInt
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class Network (
    val name: String,
    val baseAddress: Int,
    val subnetAddress: Int,
    val gatewayAddress: Int,
    val broadcastAddress: Int,
    @Id @GeneratedValue var id: Long? = null
) {
    constructor(
        name: String,
        baseAddressString: String,
        subnetAddressString: String,
        gatewayAddressString: String,
        broadcastAddressString: String) : this(name, ipStringToInt(baseAddressString), ipStringToInt(subnetAddressString),
        ipStringToInt(gatewayAddressString), ipStringToInt(broadcastAddressString))
}
