package tv.sweeney.labinventory.model

class NetworkDto (
    val name: String,
    val baseAddress: String,
    val subnetAddress: String,
    val gatewayAddress: String,
    val broadcastAddress: String
)
