package tv.sweeney.labinventory.repository

import com.arris.ssd.labinventory.model.Node
import org.springframework.data.repository.PagingAndSortingRepository

interface NodeRepository : PagingAndSortingRepository<Node, Long> {
    fun findByName(name: String): Node?
    fun findAllByOrderByName(): List<Node>

}
