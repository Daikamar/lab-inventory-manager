package tv.sweeney.labinventory.repository

import com.arris.ssd.labinventory.model.Network
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.PagingAndSortingRepository

interface NetworkRepository : PagingAndSortingRepository<Network, Long> {
    fun findAllByOrderByNameAsc(): List<Network>
    fun findAllByOrderByNameAsc(pageable: Pageable): Page<Network>
    fun findByName(name: String): Network?
    fun findAllByBaseAddress(baseAddress: Int): List<Network>
    fun findAllByBaseAddress(baseAddress: Int, pageable: Pageable): Page<Network>
    fun findAllByBaseAddressAndSubnetAddress(baseAddress: Int, subnetAddress: Int): List<Network>
    fun findAllByBaseAddressAndSubnetAddress(baseAddress: Int, subnetAddress: Int, pageable: Pageable): Page<Network>
}
