package tv.sweeney.labinventory.repository

import com.arris.ssd.labinventory.model.Lab
import org.springframework.data.repository.PagingAndSortingRepository

interface LabRepository : PagingAndSortingRepository<Lab, Long> {
    fun findByName(name: String): Lab?
    fun findAllByOrderByNameAsc(): List<Lab>
}
