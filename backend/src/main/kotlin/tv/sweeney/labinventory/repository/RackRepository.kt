package tv.sweeney.labinventory.repository

import com.arris.ssd.labinventory.model.Lab
import com.arris.ssd.labinventory.model.Rack
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
interface RackRepository : PagingAndSortingRepository<Rack, Long> {
    fun findByName(name: String): Rack?
    fun findBySize(size: Int): Rack?
    fun findByLab(lab: Lab): Rack?

    fun findAllByOrderByLabAsc(): List<Rack>
    fun findAllByOrderByLabAsc(pageable: Pageable): Page<Rack>

    fun findAllByOrderByName(): List<Rack>
}
