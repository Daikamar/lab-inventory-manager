package tv.sweeney.labinventory.controller

import com.arris.ssd.labinventory.model.Node
import com.arris.ssd.labinventory.repository.NodeRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody

@RestController
@RequestMapping("/api/nodes")
class NodeController(private val repository: NodeRepository) {

    @GetMapping("/")
    fun getAllNodes() : List<Node> = repository.findAllByOrderByName()

    @GetMapping("/{id}")
    fun getNode(@PathVariable id: Long) = repository.findById(id)

    @PostMapping("/create")
    fun createNode(@RequestBody node: Node) = repository.save(node)

    @PutMapping("/update")
    fun updateNode(@RequestBody node: Node) = repository.save(node)
}

