package tv.sweeney.labinventory.controller

import com.arris.ssd.labinventory.model.Rack
import com.arris.ssd.labinventory.repository.RackRepository
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/racks")
class RackController(private val repository: RackRepository) {

    @GetMapping("/")
    fun getAllRacks() = repository.findAllByOrderByName()

    @GetMapping("/{id}")
    fun getRack(@PathVariable id: Long) = repository.findById(id)

    @PostMapping("/create")
    fun createRack(@RequestBody rack: Rack) = repository.save(rack)

    // TODO update to verify a change in size can accommodate associated nodes
    @PutMapping("/update")
    fun updateRack(@RequestBody rack: Rack) = repository.save(rack)

    @DeleteMapping("/{id}")
    fun deleteRack(@PathVariable id: Long) = repository.deleteById(id)
}
