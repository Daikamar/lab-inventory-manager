package tv.sweeney.labinventory

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class LabInventoryApplication

fun main(args: Array<String>) {
	runApplication<LabInventoryApplication>(*args)
}
